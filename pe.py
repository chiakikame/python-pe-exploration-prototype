#################
#
# # PE import & export discovery tool (Python prototype)
#
# By Chiaki Lithium <chiakikame@gmail.com>, released under GPLv3
#
# ## Basic usage
# python pe.py name_of_pe_.exe
#
# ## Import able only, no details (Linux only)
# python pe.py name_of_pe_.exe | grep "><>" | cut -f 2,3
#
# ## Export able only, no details (Linux only)
# python pe.py name_of_pe_.dll | grep "<>>" | cut -f 2,3
#
# ## Reference
# https://msdn.microsoft.com/en-us/library/windows/desktop/ms680547(v=vs.85).aspx
#################

import sys
import struct

# Global endianess prefixer
p = lambda s: s

def read_and_report(tag, fp, fmt):
    data = fp.read(struct.calcsize(fmt))
    v = struct.unpack(fmt, data)
    if 's' not in fmt:
        r = list(map(hex, v))
    else:
        r = v
    print( "{0}: {1}".format(tag,  r))
    return v

def resolveLookupTable(fp, addressToLookupTable, isPe32Plus, importTableRVA, addressToImportSection, container):
    pos = fp.tell()
    fp.seek(addressToLookupTable)
    format = p("Q") if isPe32Plus else p("I")
    
    while True:
        value = read_and_report("\tImport lookup info", fp, format)[0]
        
        if value == 0: break
        
        ordinalFlagMask = 1 << (63 if isPe32Plus else 31)
        ordinalFlag = (value & ordinalFlagMask) != 0
        
        print("\t=> Ordinal flag: {0}".format(ordinalFlag))
        if ordinalFlag:
            ordinal = value & 0xFFFF
            print("\t=> Ordinal {0}".format(ordinal))
            container.append("#{0}".format(ordinal))
        else:
            hintNameRVA = value & 0x8FFFFFFF
            print("\t=> Hint / name RVA {0}".format(hex(hintNameRVA)))
            
            posBak = fp.tell()
            fp.seek( hintNameRVA - importTableRVA + addressToImportSection )
            read_and_report("\t\tHint", fp, p("H"))
            entryName = resolveName(fp, fp.tell())
            print("\t\t{0}".format(entryName))
            container.append(entryName)
            fp.seek(posBak)
    
    fp.seek(pos)

def resolveName(fp, addressToName):
    pos = fp.tell()
    fp.seek(addressToName)
    #print("Target address: ", hex(addressToName))
    buf = b""
    while True:
        b = fp.read(1)
        if len(b) == 0 or b == b'\0':
            break
        buf += b
    fp.seek(pos)
    return buf.decode('ascii')

def processImportTable(fp, importTableRVA, addressToImportSection, isPe32Plus, dataContainer):
    originalPos = fp.tell()
    print("Trying to process Import Table...")
    fp.seek(addressToImportSection)
    
    # Import directory table
    while True:
        print("--------")
        lookupTableRVA = read_and_report("Lookup table RVA", fp, p("I"))[0]
        if lookupTableRVA == 0:
            print("END OF DIRECTORY TABLE")
            break
        read_and_report("Timestamp", fp, p("I"))
        read_and_report("Forward chain", fp, p("I"))
        nameRVA = read_and_report("Name RVA", fp, p("I"))[0]
        read_and_report("Address table RVA", fp, p("I"))[0]
        
        dllName = resolveName(fp,
         addressToName = nameRVA - importTableRVA + addressToImportSection
         )
        print("Dll name is {0}".format(dllName))
        
        importFunctionContainer = []
        
        resolveLookupTable(fp,
        addressToLookupTable = lookupTableRVA - importTableRVA + addressToImportSection,
        isPe32Plus = isPe32Plus,
        importTableRVA = importTableRVA,
        addressToImportSection = addressToImportSection,
        container = importFunctionContainer)
        
        for func in importFunctionContainer:
            dataContainer.append((dllName, func))
        
        print("")
    
    print("END OF IMPORT TABLE")
    fp.seek(originalPos)
    
def readExportNames(fp, nameListRVA, exportTableRVA, exportTableStart, numEntries):
    originalPos = fp.tell()
    fp.seek(nameListRVA - exportTableRVA + exportTableStart)
    l = []
    
    for i in range(0, numEntries):
        nameRVA = read_and_report("Name RVA {0}".format(i), fp, p("I"))[0]
        name = resolveName(fp, nameRVA - exportTableRVA + exportTableStart)
        print("Name {0} {1}".format(i, name))
        l.append(name)
    
    fp.seek(originalPos)
    return l
    
def readExportOrdinals(fp, ordinalListActualPos, numEntries, ordinalBase):
    originalPos = fp.tell()
    fp.seek(ordinalListActualPos)
    l = []
    
    for i in range(0, numEntries):
        o = read_and_report("Ordinal {0}".format(i), fp, p("H"))[0]
        l.append(o + ordinalBase)
    
    fp.seek(originalPos)
    return l
    
def processExportTable(fp, exportTableRVA, addressToExportSection, isPe32Plus, dataContainer):
    originalPos = fp.tell()
    print("Trying to process Export Table...")
    fp.seek(addressToExportSection)
    
    print("==========")
    read_and_report("Export flag (should be 0)", fp, p("I"))
    read_and_report("Time / date stamp", fp, p("I"))
    read_and_report("Major", fp, p("H"))
    read_and_report("Minor", fp, p("H"))
    
    nameRva = read_and_report("Name RVA", fp, p("I"))[0]
    name = resolveName(fp, nameRva - exportTableRVA + addressToExportSection)
    print("DLL name: " + name)
    
    ordinalBase = read_and_report("Ordinal base", fp, p("I"))[0]
    read_and_report("# address table items", fp, p("I"))
    entryCount = read_and_report("# name table items", fp, p("I"))[0]
    read_and_report("Address table RVA", fp, p("I"))
    nameListRVA = read_and_report("Name pointer list RVA", fp, p("I"))[0]
    ordinalListRVA = read_and_report("Ordinal list RVA", fp, p("I"))[0]
    
    names = readExportNames(fp, nameListRVA, exportTableRVA, addressToExportSection, entryCount)
    ordinals = readExportOrdinals(fp, ordinalListRVA - exportTableRVA + addressToExportSection, entryCount, ordinalBase = ordinalBase)
    
    dataContainer.extend(sorted(zip(ordinals, names), key=lambda i: int(i[0]) ))
    
    print("======== End of export table")
    fp.seek(originalPos)

def main():
    if len(sys.argv) == 1:
        print("Need a PE file")
        return
        
    f = sys.argv[1]

    fp = open(f, mode="rb")

    peHeaderHint = 0x3C
    fp.seek(peHeaderHint)

    peStartLocation = read_and_report("PE starts at", fp, "I")[0]

    fp.seek(peStartLocation)

    peSignature = read_and_report("PE signature is", fp, "BBBB")

    if peSignature[0] == 80:
        p = lambda s: "<" + s
    else:
        p = lambda s: ">" + s

    peMachine = read_and_report("Machine", fp, p("H"))[0]
    peNumOfSections = read_and_report("Number of sections", fp, p("H"))[0]
    fp.seek(16, 1)

    optionalHeader = read_and_report("Optional header", fp, p("H"))[0]

    isPe32Plus = False
    if optionalHeader == 0x20b:
        print("PE32+")
        isPe32Plus = True
    elif optionalHeader == 0x10b:
        print("PE")
    else:
        print("Unknown magic")
        return
        
    jumpToImageBase = 22 if isPe32Plus else 26
    format = p("Q") if isPe32Plus else p("I")
    fp.seek(jumpToImageBase, 1)
    imageBase = read_and_report("Image base", fp, format)
        
    jumpToExportTable = 80 if isPe32Plus else 64
    fp.seek(jumpToExportTable, 1)

    exportTableRVA = read_and_report("Export table RVA", fp, p("I"))[0]
    
    fp.seek(4, 1) # skip export table size
    
    importTableRVA = read_and_report("Import table RVA", fp, p("I"))[0]

    # search for .idata
    # now move to start of section table

    jumpToSectionTable = 116 if isPe32Plus else 116
    fp.seek(jumpToSectionTable, 1)

    importContainer = []
    exportContainer = []
    
    gotImport = False
    gotExport = False

    for i in range(0, peNumOfSections):
        sectionName = read_and_report("Section name", fp, p("8s"))[0]
        virtualSize = read_and_report("Virtual size", fp, p("I"))[0]
        virtualAddress = read_and_report("Virtual address of this section", fp, p("I"))[0]
        fp.seek(4, 1)
        addressToSection = read_and_report("Address (in file) to section body", fp, p("I"))[0]
        fp.seek(16, 1)
        
        # Some application stores import table in `.idata`,
        # while others stores such info in somewhere of `.rdata`
        if gotImport == False:
            if sectionName.startswith(b'.idata') or importTableRVA == virtualAddress:
                processImportTable(fp,
                importTableRVA = importTableRVA,
                addressToImportSection = addressToSection,
                isPe32Plus = isPe32Plus,
                dataContainer = importContainer)
                gotImport = True
            elif importTableRVA > virtualAddress and importTableRVA < virtualAddress + virtualSize:
                print("Import table might in {0}".format(sectionName))
                processImportTable(fp,
                importTableRVA = importTableRVA,
                addressToImportSection = addressToSection + importTableRVA - virtualAddress,
                isPe32Plus = isPe32Plus,
                dataContainer = importContainer)
                gotImport = True
        
        if gotExport == False:
            if sectionName.startswith(b".edata") or exportTableRVA == virtualAddress:
                processExportTable(fp,
                exportTableRVA = exportTableRVA,
                addressToExportSection = addressToSection,
                isPe32Plus = isPe32Plus,
                dataContainer = exportContainer)
                gotExport = True
            elif exportTableRVA > virtualAddress and exportTableRVA < virtualAddress + virtualSize:
                print("Export table might in {0}".format(sectionName))
                processExportTable(fp,
                exportTableRVA = exportTableRVA,
                addressToExportSection = addressToSection + exportTableRVA - virtualAddress,
                isPe32Plus = isPe32Plus,
                dataContainer = exportContainer)
                gotExport = True
        
    print("SCANNING ENDS")
    
    ## Output import table
    ## Assumed that every executables need some imports
    print("><>\t#Import table")
    print("><>\t#{0}".format("PE32+" if isPe32Plus else "PE32"))
    print("><>\tLibrary\tFunction")
    for (dllName, func) in importContainer:
        print("><>\t{0}\t{1}".format(dllName, func))
        
    ## Output export table
    if len(exportContainer) != 0:
        print("<>>\t#Export table")
        print("<>>\t#{0}".format("PE32+" if isPe32Plus else "PE32"))
        print("<>>\tOrdinal\tFunction")
        for (ordinal, name) in exportContainer:
            print("<>>\t{0}\t{1}".format(ordinal, name))


if __name__ == '__main__':
    main()

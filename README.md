# PE import discovery tool (Python prototype)

By Chiaki Lithium <chiakikame@gmail.com>, released under GPLv3

This is an prototype exploratory tool for dumping import table in portable
executable (PE) files of Microsoft Windows.

Why building such a prototype? For one thing, I'd like to create a tool that can list dependency of an executable (e.g. PE) on Linux. Although `objdump` tool can accomplish such feat, it's not the best tool to use, since it lists only direct dependency of an executable. Besides, you need to extract such information manually or with a script.

Second, nice tool such as `depends.exe` is not available in Linux as a native executable.

Third, there are indeed libraries available for reading and dissecting a PE file, however, what I need is information stored in import table, not other things.

So, for a dependency walking tool for myself, I need to

* Learn format of PE file

* Implement PE file parser (for finding import table)

* Implement PE file dependency walking

## Features

* Extract `.idata` section.

* Try to find import table inside `.rdata` section

* It's easy (I hope) to follow the code with help of the reference standard and modify the code to suit your need.

## Usage

```
$ python pe.py name_of_pe_.exe
```

## Table only, no details (Linux only)

```
$ python pe.py name_of_pe_.exe | grep "><>" | cut -f 2,3
```

## Reference

* https://msdn.microsoft.com/en-us/library/windows/desktop/ms680547(v=vs.85).aspx
